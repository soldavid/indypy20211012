# IndyPy: Automate Spreadsheets with Python, OpenPyXL and Pandas

Or: How do it stop relying in M$ Excel so much

## Event link: <https://meetingplace.io/indypy/events/4798>

## Repository: <https://gitlab.com/soldavid/indypy20211012>

![Event Hero](event_hero.png)

## IndyPy

* Home Page: <https://www.meetingplace.io/indypy>

## David Sol

* AWS Community Builder
* Twitter: @soldavidcloud
