"""
Generates and Excel file with the information of the backups of the selected vaults.
This is the workfile to get the process right.
"""

# Imports
from typing import List, Tuple

import boto3
from botocore.exceptions import ClientError  # type: ignore
from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.worksheet.table import Table, TableStyleInfo
from openpyxl.styles import Alignment, Font

# Constants
ACCOUNT_ID = "039730780582"
DEFAULT_REGIONS: List[Tuple[str, List[str]]] = [
    (
        "us-east-1",
        [
            "EC2-Diario",
        ],
    ),
]
SHEET_NAMES: List[str] = [
    "Diario",
]
COLUMN_HEADERS = (
    "Client",
    "Agent",
    "Volume",
    "Hostname",
    "Private IP",
    "OS",
    "Job ID (CommCell)",
    "Status",
    "Type",
    "Start Time",
    "End Time or Current Phase",
    "Size of Application (GB)",
    "Data Transferred (GB)",
    "Data Written (GB)",
    "(Space Saving Percentage)",
    "Retained Until",
    "Associated Media",
    "Instance Id",
    "Device",
)


# Gets data from an Image
def get_image_data(ec2_client, ami_id_to_find: str):
    """Gets AMI data

    Args:
        ec2_client (EC2Client): Boto3 EC2 Client to use
        ami_id_to_find (str): AMI Id to look for

    Returns:
        ImageTypeDef: AMI information
    """
    describe_images_response = ec2_client.describe_images(
        Owners=[ACCOUNT_ID],
        ImageIds=[ami_id_to_find],
    )
    try:
        return_data = describe_images_response["Images"][0]
    except IndexError:
        print(f"Error while getting data for AMI Id: {ami_id_to_find}")
        raise
    return return_data


# Gets data from an instance
def get_instance_data(ec2_client, instance_id_to_find: str):
    """Gets the data from an EC2 instance

    Args:
        ec2_client (EC2Client): Boto3 EC2 Client to use
        instance_id_to_find (str): Id of the instance

    Returns:
        InstanceTypeDef: Instance information
    """
    try:
        describe_instance_response = ec2_client.describe_instances(
            InstanceIds=[instance_id_to_find],
        )
        return_data = describe_instance_response["Reservations"][0]["Instances"][0]
    except IndexError:
        print(f"Error while getting data for Instance Id: {instance_id_to_find}")
        raise
    return return_data


# Gets data from an AWS EBS Snapshot
def get_snapshot_data(ec2_client, snapshot_id_to_find: str):
    """Gets the data from an EBS Snapshot

    Args:
        ec2_client (EC2Client): Boto3 EC2 Client to use
        snapshot_id_to_find (str): Id from the Snapshot to look for

    Returns:
        SnapshotTypeDef: Snapshot information
    """
    describe_snapshots_response = ec2_client.describe_snapshots(
        OwnerIds=[ACCOUNT_ID],
        SnapshotIds=[snapshot_id_to_find],
    )
    try:
        return_data = describe_snapshots_response["Snapshots"][0]
    except IndexError:
        print(f"Error while getting data for Snapshot Id: {snapshot_id_to_find}")
        raise
    return return_data


# Gets data from an AWS Backup Recovery Point
def get_recovery_point_data(
    ec2_client,
    workbook_sheet: Worksheet,
    recovery_point_record,
) -> None:
    """Gets the data from an Backup Recovery Point and into a new worksheet row
       If the recovery point is for an specific snapshot it gets ignored

    Args:
        ec2_client (EC2Client): Boto3 EC2 Client to use
        workbook_sheet (Worksheet): Worksheet to write to
        recovery_point_record (RecoveryPointByBackupVaultTypeDef): Sata of the Recovery Point

    Returns:
        None: Insert the data directly into the worksheet row
    """
    # Looks for instance backups, ignores individual volumes backups
    resource_id = recovery_point_record["ResourceArn"].split("/")[1]
    # Ignores individual volumes
    if resource_id[:4] == "vol-":
        return None
    # Gets the recovery point data
    recovery_point_arn = recovery_point_record["RecoveryPointArn"]
    ami_id = recovery_point_arn.split("/")[1]
    retained_until = recovery_point_record["CalculatedLifecycle"]["DeleteAt"].strftime(
        "%Y-%m-%d %H:%M"
    )
    creation_date = recovery_point_record["CreationDate"].strftime("%Y-%m-%d %H:%M")
    completion_date = recovery_point_record["CompletionDate"].strftime("%Y-%m-%d %H:%M")
    status_name = recovery_point_record["Status"]
    # Gets the Instance data
    try:
        instance_data = get_instance_data(ec2_client, resource_id)
        instance_name = [
            tag["Value"] for tag in instance_data["Tags"] if tag["Key"] == "Name"
        ][0]
        private_ip = instance_data["PrivateIpAddress"]
    # If the instance doesn't exists drop the record
    except (IndexError, KeyError, ClientError):
        return None
    # Gets AMI data
    ami_data = get_image_data(ec2_client, ami_id)
    os_name = ami_data.get("Platform", "linux")
    # For each Snapshot gets its data
    for snapshot_data in ami_data["BlockDeviceMappings"]:
        device_name = snapshot_data["DeviceName"]
        snapshot_id = snapshot_data["Ebs"]["SnapshotId"]
        snapshot_size = snapshot_data["Ebs"]["VolumeSize"]
        # Gets Volume data of the Snapshot
        snapshot_details = get_snapshot_data(ec2_client, snapshot_id)
        volume_id = snapshot_details["VolumeId"]
        # With all of the information add a new record in the corresponding sheet
        workbook_sheet.append(
            [
                "FONACOT",
                "AWS Backup",
                volume_id,
                instance_name,
                private_ip,
                os_name,
                ami_id,
                status_name,
                workbook_sheet.title,
                creation_date,
                completion_date,
                snapshot_size,
                snapshot_size,
                snapshot_size,
                "N/A",
                retained_until,
                snapshot_id,
                resource_id,
                device_name,
            ]
        )
    return None


# Function to setup a WorkSheet with title and headers
def setup_worksheet(active_worksheet: Worksheet, title: str) -> None:
    """Prepares a worksheet with a title and column headers

    Args:
        active_worksheet (Worksheet): Worksheet to act on
        title (str): Name for the worksheet

    Returns:
        None: Acts directly on the worksheet
    """
    active_worksheet.title = title
    # WorkSheet Header
    active_worksheet.cell(row=1, column=1, value=title)  # noqa
    active_worksheet.merge_cells("A1:S1")
    # Set Style
    active_worksheet["A1"].font = Font(
        name="Courier New",
        size=20,
        bold=True,
        italic=False,
        vertAlign=None,
        underline="none",
        strike=False,
        color="FF000000",
    )
    active_worksheet["A1"].alignment = Alignment(
        horizontal="center",
        vertical="center",
        text_rotation=0,
        wrap_text=False,
        shrink_to_fit=False,
        indent=0,
    )
    # Column Headers
    active_worksheet.append(COLUMN_HEADERS)


# Creates a new workbook
if __name__ == "__main__":
    # Creates a new workbook and get first active sheet
    backups_workbook = Workbook()

    # Sets up the first worksheet
    current_worksheet = backups_workbook.active
    setup_worksheet(current_worksheet, SHEET_NAMES[0])

    # Sets up the other worksheets
    for worksheet_name in SHEET_NAMES[1:]:
        current_worksheet = backups_workbook.create_sheet()
        setup_worksheet(current_worksheet, worksheet_name)

    # For each region
    for (current_region, vaults_list) in DEFAULT_REGIONS:
        # Sets AWS Connection and Clients
        aws_session: boto3.session.Session = boto3.session.Session(
            profile_name="FON", region_name=current_region
        )
        aws_backup = aws_session.client("backup")
        aws_ec2 = aws_session.client("ec2")

        # For each vault
        for worksheet_index, current_vault in enumerate(vaults_list):
            # Select the corresponding sheet
            current_worksheet = backups_workbook.worksheets[worksheet_index]
            # Gets the recovery points from a vault
            recovery_points_response = aws_backup.list_recovery_points_by_backup_vault(
                BackupVaultName=current_vault
            )
            recovery_points = len(recovery_points_response["RecoveryPoints"])
            print(
                f"Se encontraron {recovery_points} Recovery Points en {current_vault}"
            )
            # Get all the Instances and corresponding AMIs
            for recovery_point in recovery_points_response["RecoveryPoints"]:
                get_recovery_point_data(aws_ec2, current_worksheet, recovery_point)
            # Sets the data table
            data_table = Table(
                displayName="Backups",
                ref="A2:S" + str(current_worksheet.max_row),
            )
            # Gives the data a table style
            style = TableStyleInfo(
                name="TableStyleMedium2",
                showRowStripes=True,
                showFirstColumn=False,
                showLastColumn=False,
                showColumnStripes=False,
            )
            data_table.tableStyleInfo = style
            current_worksheet.add_table(data_table)

    # Saves the new file
    backups_workbook.save("Backups.xlsx")
